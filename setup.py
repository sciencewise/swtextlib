#!/usr/bin/env python
import sys
from distutils.core import setup
from distutils.extension import Extension

VERSION = "0.1.1"


def name_to_path(mod_name, ext):
    return '%s.%s' % (mod_name.replace('.', '/'), ext)


def c_ext(mod_name, language, includes, compile_args, link_args):
    mod_path = name_to_path(mod_name, language)
    return Extension(mod_name, [mod_path], include_dirs=includes,
                     extra_compile_args=compile_args, extra_link_args=link_args)


def cython_setup(mod_names, language, includes, compile_args, link_args):
    import Cython.Distutils
    import Cython.Build
    import distutils.core

    if language == 'cpp':
        language = 'c++'
    exts = []
    for mod_name in mod_names:
        mod_path = mod_name.replace('.', '/') + '.pyx'
        e = Extension(mod_name,
                      [mod_path],
                      language=language,
                      include_dirs=includes,
                      extra_compile_args=compile_args,
                      extra_link_args=link_args)
        exts.append(e)
    distutils.core.setup(
        name='swtextlib',
        packages=['swtextlib'],
        description="Text-related algorithms for ScienceWISE",
        version=VERSION,
        package_data={"swtextlib": ["*.pxd"]},
        ext_modules=exts,
        cmdclass={'build_ext': Cython.Distutils.build_ext},
    )


def run_setup(exts):
    setup(
        name='swtextlib',
        packages=['swtextlib'],
        description="Text-related algorithms for ScienceWISE",
        version=VERSION,
        ext_modules=exts,
        install_requires=['pystemmer'],
    )


def main(modules):
    language = 'cpp'
    includes = ['.']
    compile_args = ['-O3', '-Wno-strict-prototypes']
    link_args = []
    if sys.prefix == 'darwin':
        compile_args.extend(['-mmacosx-version-min=10.8', '-stdlib=libc++'])
        link_args.append('-lc++')
    # if USE_CYTHON:
    cython_setup(modules, language, includes, compile_args, link_args)
    # else:
    exts = [c_ext(mn, language, includes, compile_args, link_args)
            for mn in modules]
    run_setup(exts)


MOD_NAMES = ['swtextlib.languages', 'swtextlib.utils',
             'swtextlib.tokenizer', 'swtextlib.stemmers',
             'swtextlib.aho_corasick', 'swtextlib.extractor']


if __name__ == '__main__':
    # USE_CYTHON = sys.argv[1] == 'build_ext'
    # USE_CYTHON = True
    main(MOD_NAMES)
