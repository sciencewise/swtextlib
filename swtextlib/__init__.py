'''
ScienceWISE text library
------------------------

Collection of base natural language processing algorithms for stemming, text normalization, and
literal extraction.
'''

from __future__ import absolute_import
from .extractor import normalize, find
