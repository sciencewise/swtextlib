# -*- coding: utf-8 -*-
# cython: c_string_encoding=utf8
from __future__ import absolute_import, unicode_literals

from cpython cimport bool

from .utils import window


NON_WORD_SYMBOLS = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'
SPACE_SYMBOLS = '\v\r\n\f\t '
# don't replace this symbols with '$' in inormalize_tags
NUMBER_SYMBOLS = '1234567890'
NOT_NORMALIZE_SYMBOLS = '.()#$-+/*^_=\\' + NUMBER_SYMBOLS


cdef class TextTag:
    '''
    Token in text: word, separate character or extracted literal.
    '''

    WORD = 'word'
    CHARACTER = 'character'
    LITERAL = 'literal'

    cdef readonly unicode value, type
    cdef readonly size_t start, end

    def __init__(self, unicode value, size_t start, size_t end, unicode type):
        self.value = value
        self.start = start
        self.end = end
        self.type = type

    def __repr__(self):
        return '<TextTag {} {}-{}: {!r}>'.format(self.type[0].upper(),
                                                 self.start, self.end, self.value)


cdef inline bool is_number_tag(TextTag tag):
    return tag.type == TextTag.CHARACTER and tag.value in NUMBER_SYMBOLS


cdef inline bool is_symbol_part_of_word(unicode letter, unicode next_letter, long current_word_len,
                                 unicode not_word_symbols=NON_WORD_SYMBOLS + SPACE_SYMBOLS):
    if letter not in not_word_symbols:
        return True

    if current_word_len > 0:
        if letter == '.' and (next_letter not in not_word_symbols or next_letter == ''):
            return True

    return False


cpdef list tokenize(unicode text):
    '''
    Tokenize the text into the TextTags
    '''
    cdef list tokens = []

    cdef unicode current_word = ''

    cdef size_t pos
    cdef unicode letter, next_letter

    text += ' '
    for pos, (letter, next_letter) in enumerate(window(text, size=2, fill='', fill_left=False)):
        if is_symbol_part_of_word(letter, next_letter, len(current_word)):
            current_word += letter
        else:
            if len(current_word) > 0:
                tokens.append(TextTag(
                    current_word, pos - len(current_word), pos,
                    TextTag.WORD
                ))
                current_word = ''
            if letter in NON_WORD_SYMBOLS:
                tokens.append(TextTag(letter, pos, pos+1, TextTag.CHARACTER))
            elif letter in SPACE_SYMBOLS:
                tokens.append(TextTag(' ', pos, pos+1, TextTag.CHARACTER))

    return tokens[:-1]


cpdef list glue_tokens(list raw_text_tags):
    '''
    Change texpp number token type from CHARACTER to WORD;
    Join 2 word, if previous word ends at start of current word;
    Glue back together the tokens of the words with apostrophes.
    TODO: This step should not exist. But we need to modify texpp to get rid of it
    '''

    cdef TextTag substitute_tag = None
    cdef bool skip_next_step = False
    cdef list text_tags = []
    cdef TextTag prev_tag = None, tag, next_tag
    for tag, next_tag in window(raw_text_tags, size=2, fill=None, fill_left=False):
        # Skip this step if on previous step 3 tags were joined
        if skip_next_step:
            skip_next_step = False
            continue

        # change type of texpp number tag from CHARACTER to WORD
        if is_number_tag(tag):
            tag = TextTag(tag.value, tag.start, tag.end, TextTag.WORD)

        if text_tags:
            prev_tag = text_tags[-1]
        else:
            prev_tag = None

        if prev_tag:
            # Join 2 word, if previous word ends at start of current word
            if (tag.type == TextTag.WORD and prev_tag.type == TextTag.WORD and
                        prev_tag.end == tag.start):
                substitute_tag = TextTag(
                    prev_tag.value + tag.value,
                    prev_tag.start, tag.end,
                    TextTag.WORD)

        # if previous check did not any substitutions
        if not substitute_tag:
            if prev_tag and next_tag:
                # Check if we are at apostrophe's position
                if tag.type == TextTag.CHARACTER and tag.value == '\'':
                    # Join apostrophe if it is in the middle if the word
                    if (prev_tag.type == TextTag.WORD and
                            (next_tag.type == TextTag.WORD or is_number_tag(next_tag))):
                        # Substitute 3 tags with 1 tag on next iteration
                        substitute_tag = TextTag(
                            prev_tag.value + tag.value + next_tag.value,
                            prev_tag.start, next_tag.end,
                            TextTag.WORD
                        )
                        skip_next_step = True

        if substitute_tag:
            text_tags[-1:] = [substitute_tag]
            substitute_tag = None
        else:
            text_tags.append(tag)

    return text_tags
