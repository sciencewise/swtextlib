# -*- coding: utf-8 -*-
# cython: c_string_encoding=utf8
from __future__ import unicode_literals, absolute_import
from cpython cimport bool
import unicodedata

from .languages import RUSSIAN, ENGLISH


def get_stemmer(basestring language, dict predefined_stems=None):
    if language == RUSSIAN:
        import Stemmer
        return Stemmer.Stemmer('russian').stemWord
    if language == ENGLISH:
        return EnglishStemmer(predefined_stems=predefined_stems).stem

    raise Exception("Unknown language '{}'".format(language))



cdef class SimplifiedPorterStemmer:
    """
    This is a simplified PorterStemmer from NLTK, adapted to Cython.
    Original code: http://www.nltk.org/_modules/nltk/stem/porter.html
    """

    cdef dict pool
    cdef frozenset vowels

    def __init__(self):
        cdef dict irregular_forms = {
            "sky" :     ["sky", "skies"],
            "die" :     ["dying"],
            "lie" :     ["lying"],
            "tie" :     ["tying"],
            "news" :    ["news"],
            "inning" :  ["innings", "inning"],
            "outing" :  ["outings", "outing"],
            "canning" : ["cannings", "canning"],
            "howe" :    ["howe"],

            # --NEW--
            "proceed" : ["proceed"],
            "exceed"  : ["exceed"],
            "succeed" : ["succeed"], # Hiranmay Ghosh

            # swtextlib
            "mars": ["mars"],
            "nitriding": ["nitriding"],
            "overring": ["overring"],
            "ranking": ["ranking", "rankings"],
            }

        self.pool = {}
        for key in irregular_forms:
            for val in irregular_forms[key]:
                self.pool[val] = key

        self.vowels = frozenset(['a', 'e', 'i', 'o', 'u'])

    cdef bool _cons(self, unicode word, int i):
        """cons(i) is TRUE <=> b[i] is a consonant."""
        if word[i] in self.vowels:
            return False
        if word[i] == 'y':
            if i == 0:
                return True
            else:
                return not self._cons(word, i - 1)
        return True

    cdef int _m(self, unicode word, int j):
        """m() measures the number of consonant sequences between k0 and j.
        if c is a consonant sequence and v a vowel sequence, and <..>
        indicates arbitrary presence,

           <c><v>       gives 0
           <c>vc<v>     gives 1
           <c>vcvc<v>   gives 2
           <c>vcvcvc<v> gives 3
           ....
        """
        cdef int n = 0
        cdef int i = 0
        while True:
            if i > j:
                return n
            if not self._cons(word, i):
                break
            i += 1
        i += 1

        while True:
            while True:
                if i > j:
                    return n
                if self._cons(word, i):
                    break
                i += 1
            i += 1
            n += 1

            while True:
                if i > j:
                    return n
                if not self._cons(word, i):
                    break
                i += 1
            i += 1

    cdef bool _vowelinstem(self, unicode stem):
        """vowelinstem(stem) is TRUE <=> stem contains a vowel"""
        for i in range(len(stem)):
            if not self._cons(stem, i):
                return True
        return False

    cdef bool _doublec(self, unicode word):
        """doublec(word) is TRUE <=> word ends with a double consonant"""
        if len(word) < 2:
            return False
        if word[-1] != word[-2]:
            return False
        return self._cons(word, len(word)-1)

    cdef bool _cvc(self, unicode word, int i):
        """cvc(i) is TRUE <=>

        a) ( --NEW--) i == 1, and word[0] word[1] is vowel consonant, or

        b) word[i - 2], word[i - 1], word[i] has the form consonant -
           vowel - consonant and also if the second c is not w, x or y. this
           is used when trying to restore an e at the end of a short word.
           e.g.

               cav(e), lov(e), hop(e), crim(e), but
               snow, box, tray.
        """
        if i == 0:
            return False  # i == 0 never happens perhaps
        if i == 1:
            return not self._cons(word, 0) and self._cons(word, 1)
        if not self._cons(word, i) or self._cons(word, i-1) or not self._cons(word, i-2):
            return False

        cdef unicode ch = word[i]
        if ch == 'w' or ch == 'x' or ch == 'y':
            return False

        return True


    cdef unicode _step1ab(self, unicode word):
        """step1ab() gets rid of plurals and -ed. e.g.

           caresses  ->  caress
           ponies    ->  poni
           sties     ->  sti
           tie       ->  tie        (--NEW--: see below)
           caress    ->  caress
           cats      ->  cat

           feed      ->  feed
           agreed    ->  agree
           disabled  ->  disable

           meetings  ->  meeting
        """
        cdef bool ed_or_ing_trimmed = False
        if word[-1] == 's':
            if word.endswith("sses"):
                word = word[:-2]
            elif word.endswith("ies"):
                if len(word) == 4:
                    word = word[:-1]
                # this line extends the original algorithm, so that
                # 'flies'->'fli' but 'dies'->'die' etc
                else:
                    word = word[:-2]
            elif word[-2] != 's':
                word = word[:-1]

        if word.endswith("ied"):
            if len(word) == 4:
                word = word[:-1]
            else:
                word = word[:-2]
        # this line extends the original algorithm, so that
        # 'spied'->'spi' but 'died'->'die' etc

        elif word.endswith("eed"):
            if self._m(word, len(word)-4) > 0:
                word = word[:-1]

        elif word.endswith("ed") and self._vowelinstem(word[:-2]):
            word = word[:-2]
            ed_or_ing_trimmed = True
        elif word.endswith("ing") and self._vowelinstem(word[:-3]):
            word = word[:-3]
            ed_or_ing_trimmed = True

        if ed_or_ing_trimmed:
            if word.endswith("at") or word.endswith("bl") or word.endswith("iz"):
                word += 'e'
            elif self._doublec(word):
                if word[-1] not in ['l', 's', 'z']:
                    word = word[:-1]
            elif (self._m(word, len(word)-1) == 1 and self._cvc(word, len(word)-1)):
                word += 'e'

        return word

    cdef unicode _step1c(self, unicode word):
        """step1c() turns terminal y to i when there is another vowel in the stem.
        --NEW--: This has been modified from the original Porter algorithm so that y->i
        is only done when y is preceded by a consonant, but not if the stem
        is only a single consonant, i.e.

           (*c and not c) Y -> I

        So 'happy' -> 'happi', but
          'enjoy' -> 'enjoy'  etc

        This is a much better rule. Formerly 'enjoy'->'enjoi' and 'enjoyment'->
        'enjoy'. Step 1c is perhaps done too soon; but with this modification that
        no longer really matters.

        Also, the removal of the vowelinstem(z) condition means that 'spy', 'fly',
        'try' ... stem to 'spi', 'fli', 'tri' and conflate with 'spied', 'tried',
        'flies' ...
        """
        if word[-1] == 'y' and len(word) > 2 and self._cons(word, len(word) - 2):
            return word[:-1] + 'i'
        else:
            return word

    cdef unicode _step5(self, unicode word):
        """step5() removes a final -e if m() > 1, and changes -ll to -l if
        m() > 1.
        """
        cdef int a
        if word[-1] == 'e':
            a = self._m(word, len(word)-1)
            if a > 1 or (a == 1 and not self._cvc(word, len(word)-2)):
                word = word[:-1]
        if word.endswith('ll') and self._m(word, len(word)-1) > 1:
            word = word[:-1]

        return word

    cpdef unicode stem(self, unicode word):
        """Should be called with lowered word."""
        ## --NLTK--
        if word in self.pool:
            return self.pool[word]

        if len(word) <= 2:
            return word # --DEPARTURE--
        # With this line, strings of length 1 or 2 don't go through the
        # stemming process, although no mention is made of this in the
        # published algorithm. Remove the line to match the published
        # algorithm.

        # If you want to add new rules for the stemmer, do it in EnglishStemmer
        # We want to hold PorterStemmer as unmodified as possible
        word = self._step1ab(word)
        word = self._step1c(word)
        # we don't need complex stems
        # word = self._step2(word)
        # word = self._step3(word)
        # word = self._step4(word)
        word = self._step5(word)
        return word


cdef class EnglishStemmer(SimplifiedPorterStemmer):
    """
    A wrapper around SimplifiedPorterStemmer that is aware of abbreviations,
    apostrophes, dashes and unicode normalization
    """
    def __init__(self, predefined_stems=None):
        super(EnglishStemmer, self).__init__()
        if predefined_stems:
            self.pool.update(predefined_stems)

    cpdef unicode stem(self, unicode word):
        cdef unicode before = word
        cdef unicode known = self.pool.get(word)
        if known:
            return known

        cdef unicode letter, new_word = ''
        cdef int upper_count = 0

        for letter in unicodedata.normalize('NFD', word):
            if unicodedata.category(letter) != 'Mn':
                new_word += letter
        word = new_word

        # Add new rules here
        # We want to hold PorterStemmer as unmodified as possible
        if word[-2:] in ("'s", "s'"):
            word = word[:-2] + "s"

        for letter in word:
            if unicodedata.category(letter) == 'Lu':
                upper_count += 1

        # heLLo -> HELLO
        # hello -> hello
        # a -> a
        # A -> A
        if upper_count > 1 or upper_count == 1 == len(word):
            # We still want to get rid of plural endings, like RNNs
            word = super(EnglishStemmer, self).stem(word).upper()
        else:
            word = super(EnglishStemmer, self).stem(word.lower())

        self.pool[before] = word
        return word
