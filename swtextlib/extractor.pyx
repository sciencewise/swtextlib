# -*- coding: utf-8 -*-
# cython: c_string_encoding=utf8
from __future__ import unicode_literals, absolute_import
from collections import deque
from io import StringIO

from .utils import window
from .tokenizer import tokenize, glue_tokens, SPACE_SYMBOLS, TextTag, NOT_NORMALIZE_SYMBOLS
from .aho_corasick import Trie


cdef class IndexedNormalizedText:
    '''
    Normalized text with information about word positions and spaces indexes
    that allows to find the position in initial text after the normalization.
    '''
    cdef unicode norm_text
    cdef dict spaces_indexes
    cdef list words_positions

    def __init__(self):
        self.norm_text = ' '
        self.spaces_indexes = {0: 0}
        self.words_positions = [(-1, -1)]

    cdef tuple get_text_position(self, size_t start, size_t end):
        start_in_text = self.words_positions[self.spaces_indexes[start] + 1][0]
        end_in_text = self.words_positions[self.spaces_indexes[end]][1]
        return start_in_text, end_in_text


cdef IndexedNormalizedText inormalize_tags(list text_tags, stem):
    '''
    Indexed normalization for list of tags
    '''
    cdef size_t spaces_count = 1
    cdef IndexedNormalizedText normalized = IndexedNormalizedText()

    text_tags = glue_tokens(text_tags)

    buff = StringIO()
    buff.write(normalized.norm_text)
    for prev_tag, tag in window(text_tags, size=2, fill=None, fill_right=False):
        # (tag.type == TextTag.WORD and prev_tag and prev_tag.end != tag.start) is for detecting
        # gaps between texpp tokens. If second token start index is not previous token end index -
        # we consider that there is something technical tex command between them and this words are
        # two different words
        if ((tag.type == TextTag.WORD and prev_tag and prev_tag.end != tag.start)
            or (tag.type == TextTag.CHARACTER and tag.value not in (SPACE_SYMBOLS + '-\''))):
                if tag.type == TextTag.CHARACTER and tag.value in NOT_NORMALIZE_SYMBOLS:
                    buff.write(tag.value + ' ')
                else:
                    buff.write('$ ')
                normalized.words_positions.append((tag.start, tag.end - 1))
                normalized.spaces_indexes[buff.tell() - 1] = spaces_count
                spaces_count += 1

        if tag.type == TextTag.WORD:
            st = stem(tag.value)
            buff.write(st + ' ')
            normalized.words_positions.append((tag.start, tag.end - 1))
            normalized.spaces_indexes[buff.tell() - 1] = spaces_count
            spaces_count += 1

    normalized.norm_text = buff.getvalue()
    return normalized


cdef IndexedNormalizedText inormalize(literal, stem):
    '''
    Indexed normalization
    '''
    cdef list tokens = tokenize(literal)
    return inormalize_tags(tokens, stem)


cpdef unicode normalize(unicode text, stem):
    '''
    Normalizes the given text, spacing out the stemmed words by specific numbers of spaces
    and keeping track of non-word symbols. The spacing allows to retrieve the position of
    each stem's original word in the text. Non-word symbols replaced by `$` function as
    stop-symbols for extractor.
    '''
    return inormalize(text, stem).norm_text


cdef list find_in_inormalized(IndexedNormalizedText normalized, trie, include_nested=False):
    accepted_literals = deque()
    cdef list found_literals = []
    cdef unicode literal
    cdef size_t start_in_text, end_in_text, start, end

    for literal, start, end in trie.find_words_in_text(normalized.norm_text):
        if literal[0] != ' ' or literal[-1] != ' ':
            continue
        start_in_text, end_in_text = normalized.get_text_position(start, end)
        found_literals.append((literal, start_in_text, end_in_text))

    if include_nested:
        for literal, start, end in found_literals:
            accepted_literals.append(TextTag(literal, start, end + 1, TextTag.LITERAL))
    else:
        for literal, start, end in found_literals:
            while accepted_literals:
                if accepted_literals[-1].start < start:
                    break
                accepted_literals.pop()
            if (not accepted_literals or accepted_literals[-1].start > start
                or accepted_literals[-1].end < end):
                accepted_literals.append(TextTag(literal, start, end + 1, TextTag.LITERAL))

    return list(accepted_literals)


def find(literals, text, stem, trie=None, include_nested=False):
    '''
    Find literals in text.

    The normal form is ' {word} ', so trie always returns indexes of spaces. All words have spaces on the right
    side. There is a matching space for every word on the left, the first and the last index of this word in original
    text. The trie returns 2 indexes of spaces - before the first word and after the last word. The space after
    the first space is situated after the first word.

    Knowing this these facts one can get indexes of the beginning of first word and the ending of last word in original text.
    '''
    if literals is not None:
        trie = Trie(literals)
    elif trie is None:
        return list()

    if not isinstance(text, unicode):
        text = text.decode('utf-8')

    cdef IndexedNormalizedText normalized = inormalize(text, stem)
    return find_in_inormalized(normalized, trie, include_nested)


def find_in_tags(literals, text_tags, stem, trie=None, include_nested=False):
    '''
    Find literals in a list of TextTags
    '''
    if literals is not None:
        trie = Trie(literals)
    elif trie is None:
        return list()

    cdef IndexedNormalizedText normalized = inormalize_tags(text_tags, stem)
    return find_in_inormalized(normalized, trie, include_nested)
