# cython: c_string_encoding=utf8
from __future__ import absolute_import, unicode_literals
from itertools import chain, repeat, islice
from cpython cimport bool

def window(seq, int size=2, int shift=1, fill=0, bool fill_left=True, bool fill_right=True):
    """ Returns a sliding window (of width n) over data from the iterable:
      s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...
    """
    it = chain(repeat(fill, shift * fill_left), iter(seq), repeat(fill, shift * fill_right))
    result = tuple(islice(it, size))
    if len(result) == size:  # `<=` if okay to return seq if len(seq) < size
        yield result
    for elem in it:
        result = result[1:] + (elem,)
        yield result
