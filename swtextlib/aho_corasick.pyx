# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from cpython cimport bool


cdef class Node:
    cdef readonly dict transitions
    cdef readonly bool is_word
    cdef readonly Node parent
    cdef readonly unicode letter
    cdef readonly Node nearest_word_by_suf_link
    cdef readonly Node suf_link

    def __init__(self, Node parent, unicode letter):
        self.parent = parent
        self.letter = letter
        self.transitions = {}
        self.is_word = False


cdef class Trie:
    cdef Node _root

    def __cinit__(self, list words):
        self._root = Node(None, '')
        self._root.suf_link = self._root
        self._root.nearest_word_by_suf_link = self._root
        cdef unicode word
        for word in words:
            self.__add_word(word)

    cdef void __add_word(self, unicode word):
        cdef Node node = self._root
        cdef unicode letter
        for letter in word:
            if letter not in node.transitions:
                node.transitions[letter] = Node(node, letter)
            node = node.transitions[letter]
        node.is_word = True

    @staticmethod
    cdef Node transition(Node node, unicode letter):
        if letter not in node.transitions:
            if node.parent is None:
                return node
            node.transitions[letter] = Trie.transition(Trie.get_suf_link(node), letter)
        return node.transitions[letter]

    @staticmethod
    cdef Node get_suf_link(Node node):
        if node.suf_link is None:
            suf_link_of_parent = Trie.get_suf_link(node.parent)
            if suf_link_of_parent == node.parent:
                return node.parent
            node.suf_link = Trie.transition(suf_link_of_parent, node.letter)
        return node.suf_link

    @staticmethod
    cdef unicode get_word(Node node):
        cdef unicode word = ''
        while node.parent is not None:
            word += node.letter
            node = node.parent
        cdef unicode reversed_word = ''.join(reversed(word))
        return reversed_word

    cdef Node _get_nearest_word_by_suf_link(self, Node node):
        if node.nearest_word_by_suf_link is None:
            suf_link = Trie.get_suf_link(node)
            if suf_link.is_word:
                node.nearest_word_by_suf_link = suf_link
            else:
                node.nearest_word_by_suf_link = self._get_nearest_word_by_suf_link(suf_link)
        return node.nearest_word_by_suf_link

    cdef list _get_words_from_node(self, node):
        cdef list words_from_node = []
        while node != self._root:
            if node.is_word:
                words_from_node.append(Trie.get_word(node))
            node = self._get_nearest_word_by_suf_link(node)
        return words_from_node

    def find_words_in_text(self, unicode text):
        cdef Node node = self._root
        cdef list words_in_text = []
        cdef list words_from_node
        cdef unicode word
        cdef size_t index = 0
        for i in range(len(text)):
            letter = text[i]
            node = Trie.transition(node, letter)
            words_from_node = self._get_words_from_node(node)
            for word in self._get_words_from_node(node):
                words_in_text.append((word, index - len(word) + 1, index))
            index += 1
        return words_in_text
