.PHONY: test clean

test:
	./setup.py build_ext
	./setup.py install
	cd tests && nosetests -s

clean:
	rm -r ./build
	rm ./swtextlib/*.cpp
	rm ./swtextlib/*.c
	rm ./swtextlib/*.html
