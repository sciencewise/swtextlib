## Example usage ##

The following pseudocode shows how to select stemmer, build Aho-Corasick automaton (and cache it) from normalized literals and extract them from text.

**Warning**: stemming and normalization are not the same! Stemming is applied to a single word while normalization handles tokenizing and stemming of any strings.

```python
from django.core.cache import cache

from swtextlib import aho_corasick
from swtextlib.extractor import find, normalize
from swtextlib.stemmers import get_stemmer

_TRIE = None
_LOCAL_TIMESTAMP = 0
stem = None

def get_stem():
    """ Get stemmer method """ 
    global stem
    if not stem:
        language = 'en'
        predefined_stems = {x: x for x in [...]}
        stem = get_stemmer(language=language, predefined_stems=predefined_stems)
    return stem


def _compute_trie():
    """ Build Aho-Corasick automaton """
    stem = get_stem()
    literals = set([normalize(literal, stem) for literal in ...])
    trie = aho_corasick.Trie(list(literals))
    return trie


def get_cached_trie():
    """ Use cached local tree or rebuild it if the external invalidation timestamp changed """
    global _TRIE, _LOCAL_TIMESTAMP
    timestamp = int(cache.get('trie_timestamp') or 0)
    if _TRIE is None or timestamp > _LOCAL_TIMESTAMP:
        _LOCAL_TIMESTAMP = int(time.time())
        _TRIE = _compute_trie()
    return _TRIE


literal_tags = find(literals, text, get_stem(), get_cached_trie())
```