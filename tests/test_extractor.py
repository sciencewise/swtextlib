# *=* coding=utf-8 *=*
from __future__ import unicode_literals, absolute_import

from nose.tools import eq_

from swtextlib import normalize, find
from swtextlib.languages import ENGLISH, RUSSIAN
from swtextlib.tokenizer import glue_tokens
from swtextlib.extractor import find_in_tags, tokenize, TextTag
from swtextlib.stemmers import get_stemmer


simple_english_stem = get_stemmer(ENGLISH)
russian_stem = get_stemmer(RUSSIAN)


def find_literals(literals, text, stem=simple_english_stem):
    normalized = [normalize(l, stem) for l in literals]
    return find(normalized, text, stem)


def test_tokenize():
    def _check(texttags, expected_text_tags):
        eq_(len(expected_text_tags), len(texttags))
        for i in xrange(len(expected_text_tags)):
            eq_(expected_text_tags[i].value, texttags[i].value)
            eq_(expected_text_tags[i].start, texttags[i].start)
            eq_(expected_text_tags[i].end, texttags[i].end)
            eq_(expected_text_tags[i].type, texttags[i].type)

    texttags = tokenize("Wick's diagram")
    expected_text_tags = [
        TextTag('Wick', 0, 4, TextTag.WORD),
        TextTag('\'', 4, 5, TextTag.CHARACTER),
        TextTag('s', 5, 6, TextTag.WORD),
        TextTag(' ', 6, 7, TextTag.CHARACTER),
        TextTag('diagram', 7, 14, TextTag.WORD)]
    _check(texttags, expected_text_tags)

    texttags = tokenize("Wick' diagram")
    expected_text_tags = [
        TextTag('Wick', 0, 4, TextTag.WORD),
        TextTag('\'', 4, 5, TextTag.CHARACTER),
        TextTag(' ', 5, 6, TextTag.CHARACTER),
        TextTag('diagram', 6, 13, TextTag.WORD)]
    _check(texttags, expected_text_tags)

    texttags = tokenize("'Wick diagram")
    expected_text_tags = [
        TextTag('\'', 0, 1, TextTag.CHARACTER),
        TextTag('Wick', 1, 5, TextTag.WORD),
        TextTag(' ', 5, 6, TextTag.CHARACTER),
        TextTag('diagram', 6, 13, TextTag.WORD)]
    _check(texttags, expected_text_tags)


def test_glue_tags():
    def _check(texttags, expected_text_tags):
        texttags = glue_tokens(texttags)
        eq_(len(expected_text_tags), len(texttags))
        for i in xrange(len(expected_text_tags)):
            eq_(expected_text_tags[i].value, texttags[i].value)
            eq_(expected_text_tags[i].start, texttags[i].start)
            eq_(expected_text_tags[i].end, texttags[i].end)
            eq_(expected_text_tags[i].type, texttags[i].type)

    tags = tokenize("Wick's theorem")
    expected_text_tags = [
        TextTag('Wick\'s', 0, 6, TextTag.WORD),
        TextTag(' ', 6, 7, TextTag.CHARACTER),
        TextTag('theorem', 7, 14, TextTag.WORD)]
    _check(tags, expected_text_tags)

    tags = tokenize("Wick' theorem")
    expected_text_tags = [
        TextTag('Wick', 0, 4, TextTag.WORD),
        TextTag('\'', 4, 5, TextTag.CHARACTER),
        TextTag(' ', 5, 6, TextTag.CHARACTER),
        TextTag('theorem', 6, 13, TextTag.WORD)]
    _check(tags, expected_text_tags)

    tags = tokenize("'Wick theorem")
    expected_text_tags = [
        TextTag('\'', 0, 1, TextTag.CHARACTER),
        TextTag('Wick', 1, 5, TextTag.WORD),
        TextTag(' ', 5, 6, TextTag.CHARACTER),
        TextTag('theorem', 6, 13, TextTag.WORD)]
    _check(tags, expected_text_tags)

    # <6>< ><dFGS> -> <6dFGS>
    tags = [
        TextTag('Galaxy', 227538, 227544, TextTag.WORD),
        TextTag(' ', 227544, 227545, TextTag.CHARACTER),
        TextTag('Survey', 227545, 227551, TextTag.WORD),
        TextTag(' ', 227551, 227552, TextTag.CHARACTER),
        TextTag('(', 227552, 227553, TextTag.CHARACTER),
        TextTag('6', 227553, 227554, TextTag.CHARACTER),
        TextTag('dFGS', 227554, 227558, TextTag.WORD),
        TextTag(')', 227558, 227559, TextTag.CHARACTER)]
    expected_text_tags = [
        TextTag('Galaxy', 227538, 227544, TextTag.WORD),
        TextTag(' ', 227544, 227545, TextTag.CHARACTER),
        TextTag('Survey', 227545, 227551, TextTag.WORD),
        TextTag(' ', 227551, 227552, TextTag.CHARACTER),
        TextTag('(', 227552, 227553, TextTag.CHARACTER),
        TextTag('6dFGS', 227553, 227558, TextTag.WORD),
        TextTag(')', 227558, 227559, TextTag.CHARACTER)]
    _check(tags, expected_text_tags)

    # <like>< ><2><MASS>< >< > -> <like>< ><2MASS>< >< >
    tags = [
        TextTag('like', 117459, 117463, TextTag.WORD),
        TextTag(' ', 117463, 117464, TextTag.CHARACTER),
        TextTag('2', 117464, 117465, TextTag.CHARACTER),
        TextTag('MASS', 117465, 117469, TextTag.WORD),
        TextTag(' ', 117469, 117470, TextTag.CHARACTER),
        TextTag(' ', 117488, 117489, TextTag.CHARACTER)]
    expected_text_tags = [
        TextTag('like', 117459, 117463, TextTag.WORD),
        TextTag(' ', 117463, 117464, TextTag.CHARACTER),
        TextTag('2MASS', 117464, 117469, TextTag.WORD),
        TextTag(' ', 117469, 117470, TextTag.CHARACTER),
        TextTag(' ', 117488, 117489, TextTag.CHARACTER)]
    _check(tags, expected_text_tags)

    # <3><d>< ><gravity> -> <3d>< ><gravity>
    tags = [
        TextTag('3', 25528, 25529, TextTag.CHARACTER),
        TextTag('d', 25529, 25530, TextTag.WORD),
        TextTag(' ', 25530, 25531, TextTag.CHARACTER),
        TextTag('gravity', 25531, 25538, TextTag.WORD)]
    expected_text_tags = [
        TextTag('3d', 25528, 25530, TextTag.WORD),
        TextTag(' ', 25530, 25531, TextTag.CHARACTER),
        TextTag('gravity', 25531, 25538, TextTag.WORD)]
    _check(tags, expected_text_tags)

    # <+><BICEP><2> -> <+><BICEP2>
    tags = [
        TextTag('+', 70165, 70166, TextTag.CHARACTER),
        TextTag('BICEP', 70166, 70171, TextTag.WORD),
        TextTag('2', 70171, 70172, TextTag.CHARACTER)]
    expected_text_tags = [
        TextTag('+', 70165, 70166, TextTag.CHARACTER),
        TextTag('BICEP2', 70166, 70172, TextTag.WORD)]
    _check(tags, expected_text_tags)

    # <NGC>< ><4><2><5><8> -> <NGC>< ><4258>
    tags = [
        TextTag('NGC', 36210, 36213, TextTag.WORD),
        TextTag(' ', 36213, 36214, TextTag.CHARACTER),
        TextTag('4', 36214, 36215, TextTag.CHARACTER),
        TextTag('2', 36215, 36216, TextTag.CHARACTER),
        TextTag('5', 36216, 36217, TextTag.CHARACTER),
        TextTag('8', 36217, 36218, TextTag.CHARACTER)]
    expected_text_tags = [
        TextTag('NGC', 36210, 36213, TextTag.WORD),
        TextTag(' ', 36213, 36214, TextTag.CHARACTER),
        TextTag('4258', 36214, 36218, TextTag.WORD)]
    _check(tags, expected_text_tags)

    # <D><0><-><branes> -> <D0><-><branes>
    tags = [
        TextTag('D', 216872, 216873, TextTag.WORD),
        TextTag('0', 216873, 216874, TextTag.CHARACTER),
        TextTag('-', 216874, 216875, TextTag.CHARACTER),
        TextTag('branes', 216875, 216881, TextTag.WORD)]
    expected_text_tags = [
        TextTag('D0', 216872, 216874, TextTag.WORD),
        TextTag('-', 216874, 216875, TextTag.CHARACTER),
        TextTag('branes', 216875, 216881, TextTag.WORD)]
    _check(tags, expected_text_tags)

    # <1><'><2> -> <1><'><2>
    tags = [
        TextTag('1', 0, 1, TextTag.CHARACTER),
        TextTag('\'', 1, 2, TextTag.CHARACTER),
        TextTag('2', 2, 3, TextTag.CHARACTER)]
    expected_text_tags = [
        TextTag('1\'2', 0, 3, TextTag.WORD)]
    _check(tags, expected_text_tags)

    # <a><'><2> -> <a'2>
    tags = [
        TextTag('a', 0, 1, TextTag.WORD),
        TextTag('\'', 1, 2, TextTag.CHARACTER),
        TextTag('2', 2, 3, TextTag.CHARACTER)]
    expected_text_tags = [
        TextTag('a\'2', 0, 3, TextTag.WORD)]
    _check(tags, expected_text_tags)

    # <1><'><b> -> <1'b>
    tags = [
        TextTag('1', 0, 1, TextTag.CHARACTER),
        TextTag('\'', 1, 2, TextTag.CHARACTER),
        TextTag('b', 2, 3, TextTag.WORD)]
    expected_text_tags = [
        TextTag('1\'b', 0, 3, TextTag.WORD)]
    _check(tags, expected_text_tags)


def test_normalization():
    eq_(normalize('WDM clusters', simple_english_stem), ' WDM cluster ')
    eq_(normalize('Clusters in WDM cosmology', simple_english_stem), ' cluster in WDM cosmologi ')
    eq_(normalize('Half-mile Telescope', simple_english_stem), ' half mile telescop ')
    eq_(normalize('Half-Mile Telescope', simple_english_stem), ' half mile telescop ')
    eq_(normalize('ARO 12m Radio Telescope', simple_english_stem), ' ARO 12m radio telescop ')
    eq_(normalize('Nançay Radio Telescope', simple_english_stem), ' nancay radio telescop ')

    eq_(normalize('UltraViolet Telescope', simple_english_stem), ' ULTRAVIOLET telescop ')
    eq_(normalize('CFHTLenS survey', simple_english_stem), ' CFHTLENS survey ')
    eq_(normalize('sTRanGE case', simple_english_stem), ' STRANGE case ')

    eq_(normalize('An Apple', simple_english_stem), ' an appl ')
    # one separate capitalized letter is special, like A-class, K-type, etc
    eq_(normalize('A Star', simple_english_stem), ' A star ')

    eq_(normalize('BHs', simple_english_stem), ' BH ')
    eq_(normalize('RHS', simple_english_stem), ' RHS ')


def test_simplest_search_english():
    literals = 'literal everywhere'.split()
    text = 'Literals are everywhere'
    tags = find_literals(literals, text)
    eq_(len(tags), 2)
    eq_(text[tags[0].start:tags[0].end], 'Literals')
    eq_(text[tags[1].start:tags[1].end], 'everywhere')


def test_simplest_search_russian():
    literals = 'яма пустырь болото катакомбы'.split()
    text = 'На пустыре болото в катакомбах бурлило, ямы заполняло'
    tags = find_literals(literals, text, russian_stem)
    eq_(len(tags), 4)
    eq_(text[tags[0].start:tags[0].end], 'пустыре')
    eq_(text[tags[1].start:tags[1].end], 'болото')
    eq_(text[tags[2].start:tags[2].end], 'катакомбах')
    eq_(text[tags[3].start:tags[3].end], 'ямы')


def test_overlapping_search():
    literals = ['warm', 'dark', 'matter', 'warm dark', 'dark matter',
                'warm dark matter']
    text = 'There are lots of warm dark matter'
    tags = find_literals(literals, text)
    eq_(len(tags), 1)
    eq_(text[tags[0].start:tags[0].end], 'warm dark matter')


def test_abbreviations():
    literals = ['WDM', 'ATLAS']
    text = ('Can WDM be found with the ATLAS Experiment? I don\'t think atlas '
            'people can help with that.')
    tags = find_literals(literals, text)
    eq_(len(tags), 2)
    eq_(text[tags[0].start:tags[0].end], 'WDM')
    eq_(text[tags[1].start:tags[1].end], 'ATLAS')


def test_more_abbreviations():
    literals = ['Accretion', 'Black hole', 'BH']
    text = ('Feedback from energy liberated by gas accretion onto black holes '
            '(BHs) is an attractive mechanism. In which the BH grows enough.')
    tags = find_literals(literals, text)
    eq_(len(tags), 4)
    eq_(text[tags[0].start:tags[0].end], 'accretion')
    eq_(text[tags[1].start:tags[1].end], 'black holes')
    eq_(text[tags[2].start:tags[2].end], 'BHs')
    eq_(text[tags[3].start:tags[3].end], 'BH')


def test_capitalization():
    literals = ['Accretion']
    text = 'Accretion'
    tags = find_literals(literals, text)
    eq_(len(tags), 1)
    eq_(text[tags[0].start:tags[0].end], 'Accretion')

    literals = ['accretion']
    text = 'accretion'
    tags = find_literals(literals, text)
    eq_(len(tags), 1)
    eq_(text[tags[0].start:tags[0].end], 'accretion')

    literals = ['Accretion']
    text = 'accretion'
    tags = find_literals(literals, text)
    eq_(len(tags), 1)
    eq_(text[tags[0].start:tags[0].end], 'accretion')


def test_text_tags():
    literals = ['Aquarius', 'Aquarius Dwarf', 'Aquarius simulation',
                'Aquarius project', 'Aquarius Stream', 'simulation']
    norm_literals = [normalize(l, simple_english_stem) for l in literals]
    text = 'From Aquarius simulations we can see the candidates, that match'
    texttags = tokenize(text)
    tags = find_in_tags(norm_literals, texttags, simple_english_stem)
    eq_(len(tags), 1)
    eq_(text[tags[0].start:tags[0].end], 'Aquarius simulations')
    eq_(tags[0].value, normalize('Aquarius simulation', simple_english_stem))


def test_dash_in_literal():

    def _check(literals, text, found_string):
        tags = find_literals(literals, text)
        eq_(len(tags), 1)
        eq_(text[tags[0].start:tags[0].end], found_string)

    def _check_not_found(literals, text):
        tags = find_literals(literals, text)
        eq_(len(tags), 0)

    # Dashes everywhere!
    _check(['N-body simulation'],
           'Improvements in N-body simulations and in hydrodynamic simulations',
           'N-body simulations')
    # Dashes in text, but not in the literal
    _check(['Line of sight velocity'],
           'The line-of-sight velocity dispersion from the binned data',
           'line-of-sight velocity')
    # Dashed in the literal, but not in text
    _check(['Line-of-sight velocity'],
           'The line of sight velocity dispersion from the binned data',
           'line of sight velocity')
    # Literal as a part of dashed word
    _check(['calibration'],
           'Adaptive self-calibration method is adopted.',
           'calibration')

    # Literal as a part of dashed word
    _check_not_found(['self-calibration'], 'Adaptive selfcalibration method is adopted.')


def test_dash_normalization():
    eq_(normalize('self-calibration', simple_english_stem), ' self calibration ')
    eq_(normalize('line-of-sight', simple_english_stem), ' line of sight ')
    eq_(normalize('galaxy-clustering', simple_english_stem), ' galaxi cluster ')


def test_apostrophe_normalization():
    eq_(normalize("Wick theorem", simple_english_stem), " wick theorem ")
    eq_(normalize("Wick's theorem", simple_english_stem), " wick theorem ")
    eq_(normalize("Wicks' theorem", simple_english_stem), " wick theorem ")
    eq_(normalize("Wicks theorem", simple_english_stem), " wick theorem ")


def test_apostrophe_string():
    tags = find_literals(["Wick theorem"],
                         "Wick theorem $ Wick's theorem $ Wicks' theorem")
    eq_(len(tags), 3)

    tags = find_literals(["Wicks theorem"],
                         "Wick theorem $ Wick's theorem $ Wicks' theorem")
    eq_(len(tags), 3)

    tags = find_literals(["Wick's theorem"],
                         "Wick theorem $ Wick's theorem $ Wicks' theorem")
    eq_(len(tags), 3)

    tags = find_literals(["Wicks' theorem"],
                         "Wick theorem $ Wick's theorem $ Wicks' theorem")
    eq_(len(tags), 3)


def test_apostrophe_in_text():
    texttags = tokenize("Wick theorem Wick's theorem Wicks' theorem")
    tags = find_in_tags([" wick theorem "], texttags, simple_english_stem)
    eq_(len(tags), 3)


def test_apostrophe_in_texpp_tags():
    texttags = [
        TextTag("Wick", 20795, 20799, "word"),
        TextTag("'", 20799, 20800, "character"),
        TextTag("s", 20800, 20801, "word"),
        TextTag(" ", 20801, 20802, "character"),
        TextTag("theorem", 20802, 20809, "word")
    ]
    tags = find_in_tags([" wick theorem "], texttags, simple_english_stem)
    eq_(len(tags), 1)


def test_articles():
    literals = ['A Star']
    text = 'This A star just exploded'

    tags = find_literals(literals, text)
    eq_(len(tags), 1)
    eq_(text[tags[0].start:tags[0].end], 'A star')

    tags = find_literals(literals, "This star is just shining.")
    eq_(len(tags), 0)

    tags = find_literals(literals, "And this is a star that's shining.")
    eq_(len(tags), 0)


def test_find():
    def _check(literal_name, text):
        tags = find([normalize(literal_name, simple_english_stem)], text, simple_english_stem)
        eq_(len(tags), 1)
        eq_(tags[0].start, text.index(literal_name))
        eq_(tags[0].end, tags[0].start + len(literal_name))

    literal_name = 'SU(2)'
    text = u'''The standard model is extended with three right-handed, singlet neutrinos with
            general couplings permitted by the $SU(2)_{L}\\times U(1)$ symmetry. The traditional
            oscillations are accounted for, as usually, by three left-handed neutrinos.
            The article investigates new structures that develop when the masses of the
            right-handed states are in the eV range. The new states interfere and oscillate
            with the standard light neutrinos. New structures appear when the detectors
            average over short wavelengths. I use these results to present and classify
            properties of the observed anomalies in the MiniBooNe, reactor and
            Gallium-detector experiments.'''
    _check(literal_name, text)


def test_find_dash():
    def _check(literal_name, text, found_text):
        tags = find([normalize(literal_name, simple_english_stem)], text, simple_english_stem)
        eq_(len(tags), 1)
        eq_(tags[0].start, text.index(found_text))
        eq_(tags[0].end, tags[0].start + len(found_text))

    _check('Meson-meson interaction', 'Meson-meson interaction', 'Meson-meson interaction')
    _check('Micro wave', 'Micro-wave', 'Micro-wave')
    _check('Micro-wave', 'Micro wave', 'Micro wave')
    _check('Information theory', 'information-theory', 'information-theory')
    _check('OPERA', 'OPERA-inspired', 'OPERA')


def test_dont_find_3C273():
    # There is a gap of 2 index lagth between C and 2.
    # No literal will be found
    literal_names = [u'3C273', u'3C 273']
    texttags = [
        TextTag(type=TextTag.WORD, start=44528, end=44535, value=u"objects"),
        TextTag(type=TextTag.CHARACTER, start=44535, end=44536, value=u" "),
        TextTag(type=TextTag.CHARACTER, start=44536, end=44537, value=u"3"),
        TextTag(type=TextTag.WORD, start=44537, end=44538, value=u"C"),
        TextTag(type=TextTag.CHARACTER, start=44540, end=44541, value=u"2"),
        TextTag(type=TextTag.CHARACTER, start=44541, end=44542, value=u"7"),
        TextTag(type=TextTag.CHARACTER, start=44542, end=44543, value=u"3")]
    norm_names = [normalize(literal_name, simple_english_stem) for literal_name in literal_names]
    tags = find_in_tags(norm_names, texttags, simple_english_stem)
    eq_(len(tags), 0)


def test_find_su2_in_text_tags():
    literal_names = [u'SU', u'SU(2)']
    texttags = [TextTag(u'This', 0, 4, TextTag.WORD),
                TextTag(u' ', 4, 5, TextTag.CHARACTER),
                TextTag(u'SU', 5, 7, TextTag.WORD),
                TextTag(u'(', 7, 8, TextTag.CHARACTER),
                TextTag(u'2', 8, 9, TextTag.CHARACTER),
                TextTag(u')', 9, 10, TextTag.CHARACTER),
                TextTag(u' ', 10, 11, TextTag.CHARACTER),
                TextTag(u'literal', 11, 18, TextTag.CHARACTER)]
    norm_names = [normalize(literal_name, simple_english_stem) for literal_name in literal_names]
    tags = find_in_tags(norm_names, texttags, simple_english_stem)
    eq_(len(tags), 1)
    tags[0].value == ' SU ( 2 ) '


def test_find_fsharp_in_text_tags():
    literal_name = 'F#'
    texttags = [TextTag(u'This', 0, 4, TextTag.WORD),
                TextTag(u' ', 4, 5, TextTag.CHARACTER),
                TextTag(u'F', 5, 6, TextTag.WORD),
                TextTag(u'#', 7, 8, TextTag.CHARACTER),
                TextTag(u' ', 8, 9, TextTag.CHARACTER),
                TextTag(u'is', 9, 11, TextTag.WORD),
                TextTag(u' ', 11, 12, TextTag.CHARACTER),
                TextTag(u'literal', 12, 19, TextTag.CHARACTER)]
    norm_name = normalize(literal_name, simple_english_stem)
    tags = find_in_tags([norm_name], texttags, simple_english_stem)
    eq_(len(tags), 1)


def test_dont_find_fsharp():
    literal_name = 'F#'
    text = 'Formula F_x is example'
    norm_name = normalize(literal_name, simple_english_stem)
    tags = find([norm_name], text, simple_english_stem)
    eq_(len(tags), 0)


def test_predefined_stems():
    literals = [normalize('Figure', simple_english_stem)]
    text = 'figuring'
    predefined_stems = {'figuring': 'figuring'}
    predefined_english_stem = get_stemmer(ENGLISH, predefined_stems)

    eq_(normalize(text, simple_english_stem), ' figur ')
    eq_(normalize(text, predefined_english_stem), ' figuring ')

    eq_(len(find(literals, text, simple_english_stem)), 1)
    eq_(len(find(literals, text, predefined_english_stem)), 0)

    text_tags = tokenize(text)
    eq_(len(find_in_tags(literals, text_tags, simple_english_stem)), 1)
    eq_(len(find_in_tags(literals, text_tags, predefined_english_stem)), 0)
