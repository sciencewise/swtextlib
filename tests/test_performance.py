import codecs
from timeit import Timer

from swtextlib import normalize, find
from swtextlib.stemmers import get_stemmer
from swtextlib.languages import ENGLISH

simple_english_stem = get_stemmer(ENGLISH)


def test_performance(n=10):
    filename = 'long_text2'
    with codecs.open(filename, u"r", u"utf8") as file:
        text = file.read()

    stem = simple_english_stem
    timing = Timer(lambda: normalize(text, stem)).timeit(number=n)

    print
    print (
        """For file {} with ~{:.3f}kB of text processing speed was {:.3f} MB/sec"""
        .format(filename, len(text) / 1024.,
                len(text) / 1024.**2 / (1. * timing / n)))

if __name__ == '__main__':
    test_performance()
