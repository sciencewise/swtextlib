# coding=utf-8
from __future__ import unicode_literals
from nose.tools import eq_

from swtextlib.stemmers import EnglishStemmer

stem = EnglishStemmer().stem


def test_nostem():
    eq_(stem('word'), 'word')


def test_plural():
    eq_(stem('words'), 'word')
    eq_(stem('boxes'), 'box')
    eq_(stem('galaxies'), 'galaxi')
    eq_(stem('galaxy'), 'galaxi')
    eq_(stem('mars'), 'mars')


def test_verbs():
    eq_(stem('polarize'), 'polariz')
    eq_(stem('polarized'), 'polariz')


def test_somethinger():
    eq_(stem('estimator'), 'estimator')
    eq_(stem('estimates'), 'estimat')


def test_ing():
    eq_(stem('over'), 'over')
    eq_(stem('polarizing'), 'polariz')

    eq_(stem('ranking'), 'ranking')
    eq_(stem('nitriding'), 'nitriding')


def test_abbr():
    eq_(stem('GALAXIES'), 'GALAXIES')
    eq_(stem('GALAXIEs'), 'GALAXIE')


def test_apostrophe():
    eq_(stem('wick'), 'wick')
    eq_(stem('wicks'), 'wick')
    eq_(stem('wicks\''), 'wick')
    eq_(stem('wick\'s'), 'wick')


def test_predefined_stems():
    # check we can pass predefined_stem as parameter
    word = 'figuring'
    eq_(stem(word), 'figur')
    sw_stemmer = EnglishStemmer(predefined_stems={word: word})
    eq_(sw_stemmer.stem(word), word)

    # check Porter pool still exists
    eq_(sw_stemmer.stem('overring'), 'overring')
